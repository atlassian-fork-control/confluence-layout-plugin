package it.com.atlassian.confluence.extra.layout;

/**
 * Tests round-trip of the layout macros through the wysiwyg converter.
 * TODO: Reenable this test once http://jira.atlassian.com/browse/RNDR-72 gets fixed.
 */
public class LayoutMacroWysiwygRoundTripFuncTestCase
{
//    private boolean isConfluence210OrNewer()
//    {
//        gotoPage("/admin/systeminfo.action");
//
//        String buildNumberStr = getElementTextByXPath("//tr/td/b[text()='Build Number']/../../td[2]");
//        int buildNumber = Integer.parseInt(buildNumberStr);
//
//        return buildNumber >= 1500;
//    }
//
//    /**
//     * Simple test that converting wiki markup containing the section macros from wiki markup
//     * to xhtml and back works correctly.  This provides some level of assurance that editing
//     * these macros in the wysiwyg editor will not break anything unless the wysiwyg editor
//     * modifies the xhtml in some way.
//     */
//    public void testRoundTripMacrosWhenConfluenceIsNewerThan210()
//    {
//        System.out.println("\n\n\n\n\nisConfluence210OrNewer() = " + isConfluence210OrNewer());
//        if (!isConfluence210OrNewer())
//            return;
//
//        String originalMarkup = "{section}\n" +
//                "{column}\n" +
//                "content1\n" +
//                "{column}\n" +
//                "{column:width=50px}\n" +
//                "{column}\n" +
//                "{column:width=10%}\n" +
//                "content2\n" +
//                "{column}\n" +
//                "{section}";
//
//        PageHelper pageHelper = getPageHelper();
//
//        pageHelper.setSpaceKey(testSpaceKey);
//        pageHelper.setTitle("testRoundTripMacros");
//        pageHelper.setContent(originalMarkup);
//
//        assertTrue(pageHelper.create());
//
//        String generatedWikiMarkup = roundTripMarkup(pageHelper);
//        assertEquals(originalMarkup, generatedWikiMarkup);
//    }
//
//    private String roundTripMarkup(PageHelper pageHelper)
//    {
//
//        String generatedXhtml = pageHelper.getContentAsXhtml();
//        String generatedWikiMarkup = pageHelper.getXhtmlAsMarkup(generatedXhtml);
//        return generatedWikiMarkup;
//    }

}
