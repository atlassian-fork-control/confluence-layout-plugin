package com.atlassian.confluence.extra.layout;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.basic.validator.WidthSizeValidator;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class ColumnMacro extends BaseMacro
{
    private static final String MACRO_NAME = "column";
    
    /**
     * There are problems with the existing layout macro conversion so this is
     * turned off for LAYOUT-11
     */
    public boolean suppressSurroundingTagDuringWysiwygRendering()
    {
        return false;
    }

    public boolean suppressMacroRenderingDuringWysiwyg()
    {
        return true;
    }

    public String getName()
    {
        return MACRO_NAME;
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        final StringBuffer outputBuffer = new StringBuffer();
        String width = StringUtils.defaultString((String) parameters.get("width"));

        if (StringUtils.isNotBlank(width))
        {
            //if there isn't a % or px on the width, render it as % by default
            if(!(width.endsWith("%") || width.endsWith("px")))
                width = outputBuffer.append(width).append("%").toString();

            // make sure width value is secure and valid
            WidthSizeValidator.getInstance().assertValid(width);
        }

        outputBuffer.setLength(0);
        outputBuffer.append("<div class=\"columnMacro\"");
        if (StringUtils.isNotBlank(width))
            outputBuffer.append(" style=\"width:").append(width).append(";min-width:").append(width).append(";max-width:").append(width).append(";\"");

        return outputBuffer.append(">")
                .append(body)
                .append("</div>")
                .toString();
    }
}
